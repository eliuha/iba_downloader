import sys
import os
import argparse
import requests
import xml.etree.ElementTree as ET

from bs4 import BeautifulSoup



def get_playlist_id(show_url, show):
    show_html = requests.get(show_url)
    encoded_html = show_html.text.encode('utf-8')
    soup = BeautifulSoup(encoded_html, 'html.parser')
    playerUrl = soup.find('div', id='playerUrl').text
    cdn_duration = soup.find('div', id = 'cdn_duration').text
    playerExt = soup.find('div', id='playerExt').text
    print 'Found details: '
    print '[playerUrl] ' + playerUrl
    print '[cdn_duration] ' + cdn_duration
    print '[playerExt] ' + playerExt
    show.playerUrl = playerUrl
    show.cdn_duration = cdn_duration
    show.playerExt = playerExt




def download_movie_chunks(url, show ):
    show.cut_playlist_url_filename()

    r = requests.get(url=url)
    filenames_to_delete = []

    playlists = r.text.splitlines()

    playlist = playlists[3] #guessing that it is on fourth line
    playlist_location = show.construct_url(playlist)
    playlist_contents = requests.get(url=playlist_location)

    chunnks = playlist_contents.text.splitlines() # count ?

    for chunnk in chunnks:
        # print chunnk
        if '#EXT' in chunnk:
            # print 'skipping'
            continue
        chunnk_location = show.construct_url(chunnk, no_token=True)

        position_of_question = chunnk.index('?')
        local_filename = chunnk[:position_of_question]
        filenames_to_delete.append(local_filename)
        print download_file(chunnk_location, local_filename)

    return filenames_to_delete


def download_file(url, filename):  #download_chunk
    # NOTE the stream=True parameter
    r = requests.get(url, stream=True)
    with open(filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                #f.flush() commented by recommendation from J.F.Sebastian
        print '[Downloaded filename] %s - file size %s' % (filename, f.tell())
    return filename

def concat_chunks(filenames_to_delete, destination_file_name):
    with open(destination_file_name, 'wb') as outfile:
        for fname in filenames_to_delete:
            with open(fname,'rb') as infile:
                for line in infile:
                    outfile.write(line)


def delete_chunks(filenames_to_delete):
    location = os.path.dirname(__file__)
    for fname in filenames_to_delete:
        delete_location = os.path.join(location, fname)
        os.remove(delete_location)



class iba_vod:
    def __init__(self):
        pass

    def cut_playlist_url_filename(self):  # cut_the_file
        position_of_playlist = self.data_chunks_url.index('playlist.m3u8')
        position_of_question_mark = self.data_chunks_url.index('?')
        self.begining = self.data_chunks_url[:position_of_playlist]
        self.end = self.data_chunks_url[position_of_question_mark:]

    def construct_url(self, file, no_token=False):
        if no_token:
            new_url = self.begining + file
        else:
            new_url = self.begining + file + self.end
        return new_url

    #def construct_cdn_url(self,resource):


    def create_metadata_url(self):
        if self.playerUrl is None:
            print 'no plater url'
            return None

        url_before = 'http://iba-metadata-rr-d.vidnt.com/vod/vod/'
        url_after = '/hls/metadata.xml?smil_profile=default'
        self.metadata_url =  url_before + self.playerUrl + url_after
        print self.metadata_url

    def parse_metadata_xml(self, filename):
        if not os.path.isfile(filename):
            print 'File not found: %s' % filename
            raise EnvironmentError('file not found %s ' % filename)
        else:
            self.metadata_xml = filename
            tree = ET.parse(self.metadata_xml)
            root = tree.getroot()
            self.title = root.find('Title').text
            links = root.find('PlaybackLinks')
            self.optianal_links = []
            for link in links:
                self.optianal_links.append({'bitrate':link.get('bitrate'), 'url':link.text})
            self.data_chunks_url = links[-1].text
            print '[Found Chunklist] %s' % self.data_chunks_url




if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=''
                                     )

    parser.add_argument('--playlist_url', help='Enter the url for the playlist.m3u' )
    parser.add_argument('--metadata_url', help='Enter the url for the metadata.xml')
    parser.add_argument('--show-url', help='Enter the url for the show itself [Not supported]')
    if len(sys.argv) != 3:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    filenames_to_delete = []
    show = iba_vod()  # remove the url from the constructor

    METADATA_XML = 'metadata.xml'
    # http://www.iba.org.il/program.aspx?scode=1996322
    if args.show_url:
        print('Using url %s' % args.show_url)
        get_playlist_id(show_url = args.show_url, show=show)
        if args.metadata_url is not None:
            show.metadata_url = args.metadata_url
        else:
            show.create_metadata_url()
            #print 'autodetected metadata url: %s' % show.metadata_url

        download_file(url=show.metadata_url, filename=METADATA_XML)
        filenames_to_delete.append(METADATA_XML)

    # http://iba-metadata-rr-d.vidnt.com/vod/vod/iba-WRXuO1-FYTS/hls/metadata.xml?smil_profile=default
    show.parse_metadata_xml(METADATA_XML)


    filenames = download_movie_chunks(url=show.data_chunks_url,show=show)

    concat_chunks(filenames, (show.title+'.'+show.playerExt))
    delete_chunks(filenames)



