# iba_downloader 
This script dowloads the movies for the iba.org,il website
IBA is an Israeli Broadcasting Authority. accessible at "http://www.iba.org.il/"
Sometimes they have interesting movies.

## Installation

    git clone git@gitlab.com:eliuha/iba_downloader.git
    cd <the folder>
    pip install requirements.txt
    
## Usage:

    python iba_downloader.py --show-url "http://www.iba.org.il/program.aspx?scode=1996322"

It will download the parts form the iba website to your working folder and then combine them to a single file.

## Thanks 
Inpired by 
- https://github.com/K-S-V/Scripts
- https://github.com/rg3/youtube-dl

Great job guys, thanks a lot !

